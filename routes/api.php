<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group([
   'prefix' => 'campaigns',
   'middleware' => 'auth-connect'
], function () {
    Route::get('/list', [\App\Http\Controllers\Api\CampaignController::class, 'index']);
    Route::post('/create', [\App\Http\Controllers\Api\CampaignController::class, 'store']);
    Route::post('/update', [\App\Http\Controllers\Api\CampaignController::class, 'update']);
    Route::post('/delete', [\App\Http\Controllers\Api\CampaignController::class, 'delete']);
});