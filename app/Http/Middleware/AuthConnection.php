<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Symfony\Component\HttpFoundation\Response;

class AuthConnection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
       try
       {
           $token = $request->bearerToken();
           $checkToken = DB::table('personal_access_tokens')->where('token', $token)->get();
           if (sizeOf($checkToken)) {
               $realToken = Redis::get('token' . '_' . $checkToken[0]->name);
               if($realToken){
                   return $next($request);
               } else {
                   return response()->json([
                       'status'    => 401,
                       'message'   => 'Unauthorized'
                   ]);
               }
           } else {
                    return response()->json([
                       'status'     => 404,
                       'message'    => 'Token not found'
                    ], 404);
           }
       } catch(\Throwable $th) {
           return response()->json([
                'status'    => 500,
                'message'   => $th->getMessage()
           ], 500);
       }
    }
}
