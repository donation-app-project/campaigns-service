<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;

class CampaignController extends Controller
{
    public function index() {
        try
        {
            $lists = Campaign::all();
            if(sizeOf($lists)) {
                return response()->json([
                    'status_code'   => 200,
                    'data'          => $lists,
                    'message'       => 'Success get list campaign'
                ]);
            } else {
                return response()->json([
                   'status_code'    => 400,
                   'data'           => null,
                   'message'        => 'Failed get list campaign'
                ], 400);
            }
        } catch(\Throwable $th) {
            return response()->json([
                'status_code'   => 500,
                'message'       => $th->getMessage()
            ], 500);
        }
    }
    public function store(Request $request) {
       try
       {
           $validator = Validator::make($request->all(), [
               'title'      => 'required',
                'for'       => 'required',
               'description'    => 'required',
           ]);

           if ($validator->fails()) {
                return response()->json([
                    'status_code'   => 400,
                    'errors'        => $validator->errors(),
                    'message'       => 'Pastikan kolom terisi dengan benar'
                ], 400);
           }

           $uid = $request->header('user_id');
           $user = DB::select("SELECT * FROM users WHERE id = ?", [$uid]);
           $token = Redis::get('token_'. $user[0]->name);
           if(sizeof($token)){
            $campaign = Campaign::create([
            'from_user'     => $uid,
            'for'       => $request->for,
            'value'         => 0.00,
            'description'   => $request->description
            ]);
            Redis::set($user[0]->name . '_campaign', $campaign);
                if($campaign){
                    return response()->json([
                        'status_code'   => 200,
                        'data'          => $campaign,
                        'message'       => "Success creating campaign"
                    ]);
                } else {
                    return response()->json([
                        'status_code'   => 400,
                        'message'       => 'Failed creating campaign'
                    ], 400);
                }
           } else {
            return response()->json([
                'status_code'   => 401,
                'message'       => 'Unauthorized'
            ], 401);
           }
       } catch (\Throwable $th) {
            return response()->json([
                'status_code'   => 500,
                'errors'        => $th->getMessage(),
                'message'       => 'Internal Server Error'
            ], 500);
       }
}

    public function update(Request $request) {
        try
        {
            $validator = Validator::make($request->all(), [
                'campaign_id'    => 'required',
                'title'          => 'nullable',
                'description'    => 'nullable'
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'status_code'   => 400,
                    'errors'        => $validator->errors(),
                    'message'       => 'Pastikan kolom terisi dengan benar'
                ], 400);
            }

            $c_id = $request->campaign_id;
            $campaign = Campaign::find($c_id);
            if (sizeOf($campaign)) {
                $campaign->update([
                    'title'         => $request->title,
                    'description'   => $request->description,
                ]);
                return response()->json([
                    'status_code'   => 200,
                    'data'          => $campaign,
                    'message'       => 'Success update campaign'
                ]);
            } else {
                return response()->json([
                    'status_code'   => 404,
                    'message'       => 'Campaign not found'
                ], 404);
            }

        } catch(\Throwable $th) {
            return response()->json([
                'status_code'   => 500,
                'message'       => $th->getMessage()
            ]);
        }
    }

    public function destroy(Request $request) {
        try
        {
            $c_id = $request->campaign_id;
            $campaign = Campaign::find($c_id);
            if(sizeOf($campaign)){
                $campaign->delete();
                if($campaign){
                    return response()->json([
                        'status_code'   => 200,
                        'message'       => 'Success delete campaign'
                    ]);
                } else {
                    return response()->json([
                        'status_code'   => 400,
                        'message'       => 'Failed delete campaign'
                    ]);
                }
            } else {
                return response()->json([
                    'status_code'   => 404,
                    'message'       => 'Campaign not found'
                ]);
            }
        } catch(\Throwable $th) {
            return response()->json([
                'status_code'   => 500,
                'message'       => $th->getMessage()
            ], 500);
        }
    }
}
